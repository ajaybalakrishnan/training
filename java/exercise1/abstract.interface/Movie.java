package com.bookmyshow.classes;
import com.bookmyshow.classes.Ticket;
public abstract class Movie{
    protected String name;
    protected int id;
    protected float rating;
    public abstract Ticket getTicket();
}