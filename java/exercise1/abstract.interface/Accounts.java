package com.bookmyshow.classes;
import com.bookmyshow.classes.User;

public interface Accounts{
    public abstract void createUser();
    public abstract void updateUser();
    public abstract void deleteUser(Customer c);
}