package com.bookmyshow.classes;
public abstract class User{
    protected String name;
    protected String dob;
    protected String phoneNumber;
    protected String mail;
    public abstract void getDetails();
}