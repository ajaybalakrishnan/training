
class Student{
    String name;
    public int rollNo;
    private String dOB;
    protected String blood;
    class Inner{
        public void print(){
            System.out.println(name);
            System.out.println(rollNo);
            System.out.println(dOB);
            System.out.println(blood);
        }
    }

    public static void main(String args[]){
        student s1=new student();
        s1.name = "aadhi";
        s1.rollNo = 007;
        s1.dOB = "29-11-2000";
        s1.blood = "B+";
        Student.Inner in = s1.new inner();
        in.print();
    }
    //All the access specifiers are accessable by the inner class;
}