import mypack.Teacher;

public class AsistantProfessor{
    public void print(teacher t){
        //System.out.println(t.name);
    // name attribute in the teacher class is a default member so it is not accessable from outside the package    
        System.out.println(t.rollNo);
        //System.out.println(dOB); 
    //here the attribute dOB is a private member so it is not accessable by the class of same package
        //System.out.println(t.blood);
    //blood attribute in the teacher class is a protected member so it is not accessable from outside the package which is not a subclass
    }
}