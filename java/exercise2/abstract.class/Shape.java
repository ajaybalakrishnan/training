package mypack;
public abstract class Shape{
    public abstract float area();
    public abstract float perimeter();
}