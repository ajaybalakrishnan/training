class Circle extends mypack.Shape{
    int radius;
    float pie = 3.14f;
    public float area(){
        return(pie*radius*radius);
    }
    public float perimeter(){
        return(2*pie*radius);
    }
}