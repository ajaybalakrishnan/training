/*  Requirement:
        TO write the code to print Fibonacci Series using for loop.
    
    Entities :
        -FibonacciForLoop

    Funcrion Declared:
        This class has no Function.
    
    Job to be done:
        - To write the code*/
package classes;

class FibonacciForLoop{
    
    public static void main(String[] args){
        int c, a = 0, b = 1,n = 10;  
        for(int i = 0;i<n;i++){
            System.out.print(a+" ");
            c=a+b;
            a=b;
            b=c;
        }
    }
}
//output:
//0 1 1 2 3 5 8 13 21 34