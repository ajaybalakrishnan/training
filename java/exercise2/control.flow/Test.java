/*  Requirements:
        The output of the code snippet should be found
    
    Entities:
        -Test
    
    Fuctions Declared:
        This class has no functions.
    
    Job to be done:
        -To print the output for the code snippet*/



//Solution

package classes;
class Test{
    public static void main(String[] args){
        int aNumber = 3;
        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
            else System.out.println("second string");
        System.out.println("third string");
    }
}