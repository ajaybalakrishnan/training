/*  Requirement:
        TO write the code to print Fibonacci Series using for Reccursion.
    
    Entities :
        -FibonacciReccursion

    Funcrion Declared:
        This class has no Function.
    
    Job to be done:
		- To write the code*/

package classes;
class FibonacciReccursion{
	public static int c,a = 0,b = 1,n =10;
	public static void print(){
		if(n<=0){
			return;
		}
		else{
			c = a + b;
			a = b;
			b = c;
			System.out.print(a+" ");
			n--;
			print();
		}
	}
	public static void main(String[] args){
		print();
	}
}