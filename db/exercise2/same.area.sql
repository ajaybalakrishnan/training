SELECT * 
    FROM `employee`
        JOIN (SELECT `area`
                    ,`department_id`
                FROM `employee`
            GROUP BY `area`, `department_id`
                HAVING COUNT(`area`)>1
                    AND COUNT(`department_id`))  AS e
        ON `employee`.`area` = e.`area`
        AND `employee`.`department_id`=`e`.`department_id` ;
        