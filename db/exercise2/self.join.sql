SELECT `a`.`name` AS `Teacher1`
      ,`b`.`name` AS `Teacher2`
      ,`a`.`sub_id`
    FROM `teachers` `A`, `teachers` `B`
WHERE `a`.`sub_id` = `b`.`sub_id` 
    AND `a`.`id` <> `b`.`id`
ORDER BY `a`.`sub_id`;
 