SELECT `department_name`
      ,`employee_id`
      ,CONCAT(`first_name`," ",`surname`) as `employee_name`
      ,`annual_salary` AS `maximum_salary`
FROM `employee`
      JOIN (SELECT DISTINCT `department_id`
                             ,MAX(`annual_salary`) AS `max_salary`
                FROM `employee`
            GROUP BY `department_id`) AS e
        ON `employee`.`department_id` = e.`department_id`
            AND `employee`.`annual_salary` = e.`max_salary`
JOIN `department`
ON `employee`.`department_id` = `department`.`department_id`;


SELECT `department_name`
      ,`employee_id`
      ,CONCAT(`first_name`," ",`surname`) as `employee_name`
      ,`annual_salary` AS `minimum_salary`
FROM `employee`
      JOIN (SELECT DISTINCT `department_id`
                             ,MIN(`annual_salary`) AS `min_salary`
                FROM `employee`
            GROUP BY `department_id`) AS e
        ON `employee`.`department_id` = e.`department_id`
            AND `employee`.`annual_salary` = e.`min_salary`
JOIN `department`
ON `employee`.`department_id` = `department`.`department_id`;
            
            
