CREATE TABLE `my_db.employee` (
  `id` INT NOT NULL
  ,`first_name` VARCHAR(45) NOT NULL
  ,`surname` VARCHAR(45) NULL
  ,`dob` DATE NOT NULL
  ,`date_of_joining` DATE NOT NULL
  ,`annual_salary` FLOAT NOT NULL
  ,`dept_id` INT NULL
  ,PRIMARY KEY (`id`));

CREATE TABLE `department`(
	`id` INT NOT NULL
    ,`name` VARCHAR(45)
    ,PRIMARY KEY(`id`));
