ALTER TABLE `teachers` 
    RENAME COLUMN `rollNo` to `id`;
    
ALTER TABLE `teachers`
    ADD COLUMN `date_of_joining` DATE
    ,ADD COLUMN `salary` INT
    ,ADD COLUMN `sub_id` INT
    ,ADD COLUMN `dob` DATE;

ALTER TABLE `teachers`
    DROP COLUMN `dob`;
