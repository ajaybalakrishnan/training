ALTER TABLE `teachers`
   MODIFY `name` VARCHAR(45) NOT NULL
   ,MODIFY `class` INT NOT NULL
   ,MODIFY `date_of_joining` DATE NOT NULL
   ,ADD CONSTRAINT `MyPrimaryKey` PRIMARY KEY (`id`)
   ,ADD CONSTRAINT `MyForiegnKey` FOREIGN KEY(`sub_id`) 
        REFERENCES subject(`id`);

ALTER TABLE `subjects`
    ADD CONSTRAINT MyPrimaryKey PRIMARY KEY (`id`);

ALTER TABLE `subjects`
    MODIFY `name` VARCHAR(45) NOT NULL;

    