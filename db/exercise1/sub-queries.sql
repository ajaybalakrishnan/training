SELECT * FROM `teachers`
    INNER JOIN `subjects`
        ON `teachers`.`sub_id` = `subjects`.`id`
            WHERE `subjects`.`name` IN (SELECT `name` FROM `subjects` WHERE `name` IN ('mathematics','english'));
