CREATE VIEW `my_view` AS
    SELECT `name`, `sub_id` FROM `teachers`
        WHERE `salary` > 300000;

SELECT * FROM `my_view`;