ALTER TABLE `teachers`
    ADD COLUMN `bonus` int;
    
SET SQL_SAFE_UPDATES=0;

UPDATE `teachers`
    SET `bonus` = 4000;

UPDATE `teachers`
    SET `bonus` = 4000 * 1.1
        WHERE `id` = 3 OR `id` = 2;
