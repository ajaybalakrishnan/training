SELECT * FROM `teachers`
    INNER JOIN `subjects`
        ON `teachers`.`sub_id` = `subjects`.`id`;

SELECT * FROM `teachers`
    RIGHT JOIN `subjects`
        ON `teachers`.`sub_id` = `subjects`.`id`;
        
SELECT * FROM `teachers`
    LEFT JOIN `subjects`
        ON `teachers`.`sub_id` = `subjects`.`id`;

SELECT * FROM `teachers`
    JOIN `subjects`
        ON `teachers`.`sub_id` = `subjects`.`id`;