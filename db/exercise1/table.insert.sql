INSERT INTO `teachers`(`id`,`name`,`class`,`date_of_joining`,`salary`,`sub_id`)
VALUES
    (001,'Devi',9,'2019-07-12',300000,2)
    ,(002,'Aruna',9,'2016-06-19',450000,3)
    ,(003,'Karthika',8,'2018-03-2',400000,3)
    ,(004,'Joicy',10,'2019-05-12',300000,2)
    ,(005,'Jamuna',10,'2018-11-26',350000,4)
    ,(006,'Sasi',10,'2019=12=30',350000,5);

INSERT INTO `teachers`(`id`,`name`,`class`,`date_of_joining`,`salary`)
VALUES
    (007,'Banu',1,'2020-1-1',250000);
    
INSERT INTO `students`(`rollNo`,`name`,`class,blood`)
VALUES
    (001, 'Asiq',2,'B+')
    ,(002,'Ashok',3,'A+')
    ,(003,'Bharath',5,'AB-')
    ,(004,'Nithish',8,'O+');