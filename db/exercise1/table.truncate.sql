INSERT INTO `subjects`(`id`,`name`)
VALUES
    (1,'tamil')
    ,(2,'english')
    ,(3,'mathematics')
    ,(4,'science')
    ,(5,'social science');
    
INSERT INTO `teachers`(`id`,`name`,`class`,`date_of_joining`,`salary`,`sub_id`)
VALUES
    (001,'Devi',1,'2019-07-12',300000,2)
    ,(002,'Aruna',2,'2016-06-19',450000,3)
    ,(003,'Karthika',3,'2018-03-2',40000,3);

TRUNCATE TABLE `teachers`;