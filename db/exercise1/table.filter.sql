SELECT  name , `salary` FROM `teachers`
    WHERE `id` IS NOT NULL and `sub_id` = 3;
    
SELECT  * FROM `teachers`
    WHERE `sub_id` = ANY (SELECT `id` FROM `subjects` WHERE `name` = 'mathematics');
    
SELECT  * FROM `teachers` 
    WHERE `name` LIKE '%na%';

SELECT * FROM `teachers`
    WHERE `name` LIKE 'a_%';

SELECT * FROM `teachers`
    WHERE `sub_id` in (1,3,5);
    