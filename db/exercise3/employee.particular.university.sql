SELECT `employee`.`id`
      ,`employee`.`name`
      ,`employee`.`dob`
      ,`employee`.`email`
      ,`employee`.`phone`
      ,`college`.`name` AS `collage_name`
      ,`department`.`dept_name` AS `department_name`
    FROM `employee`
        JOIN `college`
            ON `college`.`id` = `employee`.`college_id`
        JOIN `college_department`
            ON `college_department`.`college_id` = `college`.`id`
        JOIN `department`
            ON `department`.`dept_code` = `college_department`.`udept_code`
        JOIN `university`
            ON `university`.`univ_code` = `department`.`univ_code`
        JOIN `designation`
            ON `designation`.`id` = `employee`.`desig_id`
WHERE `university`.`university_name` = "Anna University"
GROUP BY `employee`.`id`
ORDER BY `designation`.`rank` AND `university`.`university_name`;