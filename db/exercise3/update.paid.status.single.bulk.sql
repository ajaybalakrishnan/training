UPDATE `semester_fee`
SET `paid_status` = 'Paid',
    `paid_year` = '2019'
WHERE `stud_id` = (SELECT `id`
                    FROM `student`
                    WHERE `roll_number` = '318it005')
AND `semester` = 1;


SET SQL_SAFE_UPDATES = 0;
UPDATE `semester_fee`
SET `paid_status` = 'Paid',
    `paid_year` = '2020'
WHERE `stud_id` IN (SELECT `id`
                    FROM `student`
                    WHERE `roll_number` IN ('318it005',
                                            '117ce002',
                                            '118ce003',
                                            '118ce010'))
AND `semester` = 3;


