SELECT `student`.`roll_number`
      ,`student`.`name`
      ,`student`.`gender`
      ,`department`.`dept_name`
      ,`college`.`name`
      ,`r`.`grade`
      ,`r`.`credits`
      ,`r`.`semester`
    FROM `student`
    JOIN `college_department`
        ON `college_department`.`cdept_id` = `student`.`cdept_id`
    JOIN `department`
        ON `department`.`dept_code` = `college_department`.`udept_code`
    JOIN (SELECT  `stud_id`
                ,`grade`
                ,`credits`
                ,`semester`
            FROM `semester_result`
          WHERE `credits`> 5 AND `semester` = 1) AS r 
        ON `r`.`stud_id` = `student`.`id`
    JOIN `college`
        ON `college`.`id` = `student`.`college_id`
ORDER BY `college`.`name` AND `r`.`semester`
;