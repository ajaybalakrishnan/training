SELECT `student`.`roll_number`
      ,`student`.`name`
      ,`student`.`gender`
      ,`department`.`dept_name`
      ,`college`.`name`
      ,`semester_result`.`grade`
      ,`semester_result`.`credits`
      ,`semester_result`.`semester`
    FROM `student`
    JOIN `college_department`
        ON `college_department`.`cdept_id` = `student`.`cdept_id`
    JOIN `department`
        ON `department`.`dept_code` = `college_department`.`udept_code`
    JOIN `semester_result`
        ON `semester_result`.`stud_id` = `student`.`id`
    JOIN `college`
        ON `college`.`id` = `student`.`college_id`
ORDER BY `college`.`name` AND `semester_result`.`semester`;